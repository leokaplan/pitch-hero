
local Moan = require "moan"

local samples = {
	sin = {},
	triangle = {},
	saw = {},
	rect = {},
}
local defaults = {
	sin = {
		osc    = Moan.osc.sin,
		octave = 4,
		amp    = .6,
		next   = 'triangle',
		color  = {40, 80, 190},
	},
	triangle = {
		osc    = Moan.osc.triangle,
		octave = 5,
		amp    = .6,
		next   = 'saw',
		color  = {40, 190, 80},
	},
	saw = {
		osc    = Moan.osc.saw,
		octave = 3,
		amp    = .3,
		next   = 'rect',
		color  = {190, 190, 50},
	},
	rect = {
		osc    = Moan.osc.rect,
		octave = 2,
		amp    = .3,
		next   = 'sin',
		color  = {190, 40, 40},
	},
}

trumpet = {}
function trumpet.load()
	love.graphics.setBackgroundColor(30,30,30)
end
function trumpet.draw()
    W,H = love.window.getDimensions()
    n = 25 
    cellw,cellh = W,H/n
    local color = love.graphics.setColor
    for i=1,n do
        color(255,255,255)
        if i%4 == 0 then
            color(0,255,0,50)
        end
        if i%3 == 0 then
            color(0,0,255,50)
        end
        if i%12 == 0 then
            color(255,0,0,50)
        end
        love.graphics.rectangle("fill",0,i*cellh,cellw,(i+1)*cellh)
        color(0,0,0)
        love.graphics.rectangle("line",0,i*cellh,cellw,(i+1)*cellh)
    end
    --sources
    osc = defaults.sin
end

local function createSample(osc, pitch, amp, octave)
	local len = .5
	return Moan.newSample(Moan.compress(Moan.envelope(
		osc(Moan.pitch(pitch, octave), amp),
		Moan.osc.triangle(8),
		Moan.env.adsr(len/10, 3*len/10, 5*len/10, len/10, amp*1.2, amp))), len)
end

local pitchtable = {'b','a#','a','g#','g','f#','f','e','d#','d','c#','c'}
function trumpet.update(dt)
	if love.mouse.isDown("l") then
	    local cy = math.floor(love.mouse.getY()/cellh) 
        local p = cy % #pitchtable + 1
        local a = math.floor(cy / #pitchtable)
        local pitch = pitchtable[p]
--        print(p,a)
        if not samples[pitch] then
            samples[pitch] = {}
        end
        if not samples[pitch][a] then
                samples[pitch][a] = createSample(osc.osc,pitch, osc.amp, osc.octave - a)
        end
        
		source = love.audio.newSource(samples[pitch][a])
        
        if source then
            love.audio.play(source)--s[discrete(love.mouse.getY())])
        end
    end
end
return trumpet
